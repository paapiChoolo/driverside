package com.example.syyam.driverlocation.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.syyam.driverlocation.API;
import com.example.syyam.driverlocation.Adapters.DriverShipmentOrderAdapter;
import com.example.syyam.driverlocation.Models.OrderStatusDTO;
import com.example.syyam.driverlocation.Models.StatusData;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.Utils.Config;

import java.io.ByteArrayOutputStream;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class confirmCancelActivity extends AppCompatActivity {

    private LinearLayout cameraBtn;
    private EditText reason;
    private Button cancel, submit;
    private ImageButton cameraBtnForReal;
    String orderNumber, type, waveNumber;
    Bitmap bitmap;
    private String encodedImage;

    RelativeLayout rootLayout;

    ImageView imageView;


    public void onBackPressed() {
        Intent L = new Intent(confirmCancelActivity.this, DriverShipmentOrderDetailActivity.class);
        L.putExtra("waveNumber", waveNumber);
        L.putExtra("data", orderNumber);
        startActivity(L);
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_cancel);
        setTitle("Submit Details");

        Intent iin = getIntent();
        Bundle b = iin.getExtras();

        if (b != null) {
            orderNumber = (String) b.get("orderNumber");
            waveNumber = (String) b.get("waveNumber");
            type = (String) b.get("type");
        }

        cameraBtn = (LinearLayout) findViewById(R.id.cameraBtn);
        reason = (EditText) findViewById(R.id.reason);
        cancel = (Button) findViewById(R.id.cancel);
        cameraBtnForReal = (ImageButton) findViewById(R.id.cameraBtnForReal);
        submit = (Button) findViewById(R.id.submit);

        rootLayout = (RelativeLayout) findViewById(R.id.rootLayoutt);

        submit.setEnabled(false);
        submit.setTextColor(Color.parseColor("#bcbcbc"));

        imageView = findViewById(R.id.imageView);

        cameraBtnForReal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 0);
            }
        });
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 0);
                }


            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                doStuff(orderNumber);

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent L = new Intent(confirmCancelActivity.this, DriverShipmentOrderDetailActivity.class);
                L.putExtra("waveNumber", waveNumber);
                L.putExtra("data", orderNumber);
                startActivity(L);
                finish();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && resultCode == RESULT_OK) {
            submit.setEnabled(true);
            submit.setTextColor(Color.parseColor("#57d8ca"));
            bitmap = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(bitmap);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            encodedImage = Base64.encodeToString(byteArray, Base64.NO_WRAP);

        }
    }

    private void doStuff(String orderNumberr) {
        String Reason = reason.getText().toString();

        if (type.equals("cancel")) {
            if (TextUtils.isEmpty(Reason)) {
                final Snackbar snackbar = Snackbar.make(rootLayout, "Please Enter A Note", Snackbar.LENGTH_LONG);
                snackbar.setAction("DISMISS", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });
                snackbar.show();

                return;
            }
            if (Reason.length() < 6) {
                final Snackbar snackbar = Snackbar.make(rootLayout, "Note Is Not Long Enough!", Snackbar.LENGTH_LONG);
                snackbar.setAction("DISMISS", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });
                snackbar.show();
                return;
            }
        }


        //Loader
        final SpotsDialog waitingDialog = new SpotsDialog(confirmCancelActivity.this);
        waitingDialog.show();

        //...

        Retrofit build = new Retrofit
                .Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API retrofitController = build.create(API.class);

        StatusData statusData = new StatusData();
        statusData.setOrderNumber(orderNumberr);
        statusData.setReason(Reason);
        if (type.equals("cancel")) {
            statusData.setStatus("Cancelled");
        } else
            statusData.setStatus("Delivered");

        statusData.setInvoicePicture(encodedImage);

        Call<OrderStatusDTO> wave = retrofitController.DriverShipmentOrderStatus(Config.getToken(confirmCancelActivity.this), statusData);
        wave.enqueue(new Callback<OrderStatusDTO>() {
            @Override
            public void onResponse(Call<OrderStatusDTO> call, Response<OrderStatusDTO> response) {
                Toast.makeText(confirmCancelActivity.this, response.body().getMsg(), Toast.LENGTH_LONG).show();
                if (response.isSuccessful()) {
                    waitingDialog.dismiss();
                    if (response.body() != null) {
                        Intent L = new Intent(confirmCancelActivity.this, DriverShipmentOrderDetailActivity.class);
                        L.putExtra("waveNumber", waveNumber);
                        L.putExtra("data", orderNumber);
                        startActivity(L);
                        finish();
                    } else {
                        waitingDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderStatusDTO> call, Throwable t) {
                waitingDialog.dismiss();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_home) {
            Intent back = new Intent(confirmCancelActivity.this, MainActivity.class);
            startActivity(back);
            finish();
        }
        if (item.getItemId() == R.id.action_back) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}

