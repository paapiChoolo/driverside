package com.example.syyam.driverlocation.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShipmentOrderPicks {

@SerializedName("OrderNumber")
@Expose
private String orderNumber;
@SerializedName("CartonLabel")
@Expose
private String cartonLabel;

public String getOrderNumber() {
return orderNumber;
}

public void setOrderNumber(String orderNumber) {
this.orderNumber = orderNumber;
}

public String getCartonLabel() {
return cartonLabel;
}

public void setCartonLabel(String cartonLabel) {
this.cartonLabel = cartonLabel;
}

}