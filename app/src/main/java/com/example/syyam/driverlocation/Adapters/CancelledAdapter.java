package com.example.syyam.driverlocation.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syyam.driverlocation.Models.Delivered;
import com.example.syyam.driverlocation.Models.WaveNumber;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.activities.AssignedWaves;
import com.example.syyam.driverlocation.activities.CancelledActivity;

import java.util.Collections;
import java.util.List;

public class CancelledAdapter extends RecyclerView.Adapter<CancelledAdapter.MyViewHolder>{
    private LayoutInflater inflater;
    List<Delivered> delivered= Collections.emptyList();
    CancelledActivity activity;

    public CancelledAdapter(CancelledActivity context, List<Delivered> delivered) {
        inflater=LayoutInflater.from(context);
        this.delivered=delivered;
        this.activity=context;
    }

    @NonNull


    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cancelled_commenttt_row, parent, false);
//        View view =inflater.inflate(R.layout.commentttt_row,parent,false);
        CancelledAdapter.MyViewHolder holder=new CancelledAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Delivered current= delivered.get(position);
        holder.number.setText("Order Number: "+current.getOrderNumber());
        holder.status.setText("Status: "+current.getUser1());

//        holder.prLinear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                activity.method1(current.getWaveNumber());
//            }
//        });
    }


    @Override
    public int getItemCount() {
        return delivered.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        LinearLayout prLinear;
        TextView number;
        TextView status;

        public MyViewHolder(View itemView) {
            super(itemView);

            number=itemView.findViewById(R.id.number);
            status=itemView.findViewById(R.id.status);
            prLinear=itemView.findViewById(R.id.prlinear);
        }
    }

}