package com.example.syyam.driverlocation.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WavesDTO {

@SerializedName("WaveNumber")
@Expose
private List<WaveNumber> waveNumber = null;

public List<WaveNumber> getWaveNumber() {
return waveNumber;
}

public void setWaveNumber(List<WaveNumber> waveNumber) {
this.waveNumber = waveNumber;
}

}