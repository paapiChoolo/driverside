package com.example.syyam.driverlocation.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syyam.driverlocation.API;
import com.example.syyam.driverlocation.Adapters.AssignedWavesAdapter;
import com.example.syyam.driverlocation.Adapters.mainAdapter;
import com.example.syyam.driverlocation.Models.RawTokenBody;
import com.example.syyam.driverlocation.Models.WaveNumber;
import com.example.syyam.driverlocation.Models.WavesDTO;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.Utils.Config;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AssignedWaves extends AppCompatActivity {
    List<WaveNumber> waveNumber;
    private LinearLayoutManager mLayoutManager;
    private TextView noItemss;
    private AssignedWavesAdapter adapter;
    private RecyclerView recyclerView;

    ProgressDialog dialoag;

    @Override
    public void onBackPressed() {
        Intent L = new Intent(AssignedWaves.this, MainActivity.class);
        startActivity(L);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assigned_waves);
        setTitle("Assigned Waves");

        noItemss = (TextView) findViewById(R.id.noItemss);
        noItemss.setVisibility(View.GONE);

        dialoag = new ProgressDialog(AssignedWaves.this);
        dialoag.setMessage("Loading Data");
        dialoag.setCanceledOnTouchOutside(false);
        dialoag.show();

        recyclerView = findViewById(R.id.recyclerViewAssigned);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        getData();
        adapter = new AssignedWavesAdapter(AssignedWaves.this, waveNumber);
    }

    public void method1(String waveNumber) {
        Intent n = new Intent(AssignedWaves.this, DriverShipmentOrderActivity.class);
        n.putExtra("type", "Assign");
        n.putExtra("data", waveNumber);
        startActivity(n);
        finish();

    }


    private void getData() {
        Log.wtf("assigned", "1 ");
        Retrofit build = new Retrofit
                .Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        API retrofitController = build.create(API.class);

        Call<WavesDTO> wave = retrofitController.DriverWavesAssigned(Config.getToken(AssignedWaves.this));
        wave.enqueue(new Callback<WavesDTO>() {
            @Override
            public void onResponse(Call<WavesDTO> call, Response<WavesDTO> response) {
                dialoag.dismiss();
                if (response.isSuccessful()) {


                    if (!response.body().getWaveNumber().isEmpty()) {
                        waveNumber = response.body().getWaveNumber();
                        AssignedWavesAdapter adapter = new AssignedWavesAdapter(AssignedWaves.this, waveNumber);
                        recyclerView.setAdapter(adapter);


                    } else {
                        noItemss.setVisibility(View.VISIBLE);
                    }

                }
                else
                    noItemss.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<WavesDTO> call, Throwable t) {
                noItemss.setVisibility(View.VISIBLE);

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.openassign_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==R.id.action_back)
        {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
