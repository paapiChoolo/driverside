package com.example.syyam.driverlocation;

import com.example.syyam.driverlocation.Entitity.Locations;
import com.example.syyam.driverlocation.Models.CancelledDTO;
import com.example.syyam.driverlocation.Models.DriverShipmentOrderDTO;
import com.example.syyam.driverlocation.Models.DriverShipmentOrderDetailDTO;
import com.example.syyam.driverlocation.Models.DriverShipmentPicksDTO;
import com.example.syyam.driverlocation.Models.LocationsDTO;
import com.example.syyam.driverlocation.Models.LoginDTO;
import com.example.syyam.driverlocation.Models.LoginData;
import com.example.syyam.driverlocation.Models.OrderStatusDTO;
import com.example.syyam.driverlocation.Models.RawTokenBody;
import com.example.syyam.driverlocation.Models.ScanOrderDTO;
import com.example.syyam.driverlocation.Models.StatusData;
import com.example.syyam.driverlocation.Models.WavesDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API {

    @POST("DriverLogin")
    Call<LoginDTO> postUser(@Body LoginData requestBody);

    @POST("DriverWaves")
    Call<WavesDTO> DriverWaves(@Header("Authorization") String token, @Body RawTokenBody rawTokenBody);

    @GET("DriverPickedWaves")
    Call<WavesDTO> DriverWavesAssigned(@Header("Authorization") String token);

    @GET("DriverShipmentOrder")
    Call<DriverShipmentOrderDTO> DriverShipmentOrder(@Header("Authorization") String token, @Query("WaveNumber") String number, @Query("Check") String check);

    @GET("DriverScanOrder")
    Call<ScanOrderDTO> DriverScanOrder(@Header("Authorization") String token, @Query("WaveNumber") String number, @Query("CartonLabel") String carton, @Query("OrderNumber") String orderNumber) ;

    @GET("DriverShipmentOrderDetail")
    Call<DriverShipmentOrderDetailDTO> DriverShipmentOrderDetail(@Header("Authorization") String token, @Query("OrderNumber") String order, @Query("WaveNumber") String number) ;

    @POST("DriverShipmentOrderStatus")
    Call<OrderStatusDTO> DriverShipmentOrderStatus(@Header("Authorization") String token, @Body StatusData statusData);

    @GET("DriverShipmentPicks")
    Call<DriverShipmentPicksDTO> DriverShipmentPicks(@Header("Authorization") String token, @Query("OrderNumber") String order, @Query("WaveNumber") String wave) ;

    @GET("DriverCancelOrders")
    Call<CancelledDTO> DriverCancelOrders(@Header("Authorization") String token) ;

    @GET("DriverDeliveredOrders")
    Call<CancelledDTO> DriverDeliveredOrders(@Header("Authorization") String token) ;

    @POST("DriverLocation")
    Call<LocationsDTO> DriverLocation(@Header("Authorization") String token, @Body SendLocation data);


}
