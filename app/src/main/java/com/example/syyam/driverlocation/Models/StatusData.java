package com.example.syyam.driverlocation.Models;

public class StatusData {

    private String OrderNumber;
    private String Status;
    private String Reason;
    private String InvoicePicture;

    public String getInvoicePicture() {
        return InvoicePicture;
    }

    public void setInvoicePicture(String invoicePicture) {
        InvoicePicture = invoicePicture;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }
}
