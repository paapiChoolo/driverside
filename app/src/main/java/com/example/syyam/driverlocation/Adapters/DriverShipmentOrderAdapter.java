package com.example.syyam.driverlocation.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syyam.driverlocation.Models.ShipmentOrder;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.activities.DriverShipmentOrderActivity;

import java.util.Collections;
import java.util.List;

public class DriverShipmentOrderAdapter extends RecyclerView.Adapter<DriverShipmentOrderAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    List<ShipmentOrder> shipmentOrders= Collections.emptyList();
    DriverShipmentOrderActivity activity;

    public DriverShipmentOrderAdapter(DriverShipmentOrderActivity context, List<ShipmentOrder> shipmentOrders) {
        inflater=LayoutInflater.from(context);

        this.shipmentOrders=shipmentOrders;
        this.activity=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.drivershipment_comentttt_row, parent, false);
//        View view =inflater.inflate(R.layout.commentttt_row,parent,false);
        DriverShipmentOrderAdapter.MyViewHolder holder=new DriverShipmentOrderAdapter.MyViewHolder(view);
        return holder;
    }

    @NonNull




    @Override
    public void onBindViewHolder(@NonNull DriverShipmentOrderAdapter.MyViewHolder holder, int position) {
        final ShipmentOrder current= shipmentOrders.get(position);
        holder.number.setText("Order Number: "+current.getOrderNumber());
        holder.status.setText("Status: "+current.getStatus());

        holder.prLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                activity.method1(current.getOrderNumber());
            }
        });

        if(shipmentOrders.size()==0){
            activity.method2();
        }
    }


    @Override
    public int getItemCount() {
        return shipmentOrders.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        LinearLayout prLinear;
        TextView number;
        TextView status;

        public MyViewHolder(final View itemView) {
            super(itemView);

            number=itemView.findViewById(R.id.OrderNumber);
            status=itemView.findViewById(R.id.status);
            prLinear=itemView.findViewById(R.id.prlinear);
        }
    }
}
