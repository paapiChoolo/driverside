package com.example.syyam.driverlocation.Entitity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "locations")
public class Locations {


    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name="longitude")
    private String Longitude;

    @ColumnInfo(name="latitude")
    private String Latitude;

    @ColumnInfo(name="battery")
    private String Battery;

    @ColumnInfo(name="time")
    private String Time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getBattery() {
        return Battery;
    }

    public void setBattery(String battery) {
        Battery = battery;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }
}
