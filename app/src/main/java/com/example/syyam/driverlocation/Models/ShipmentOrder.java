package com.example.syyam.driverlocation.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShipmentOrder {

@SerializedName("OrderNumber")
@Expose
private String orderNumber;
@SerializedName("Status")
@Expose
private String status;

public String getOrderNumber() {
return orderNumber;
}

public void setOrderNumber(String orderNumber) {
this.orderNumber = orderNumber;
}

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}