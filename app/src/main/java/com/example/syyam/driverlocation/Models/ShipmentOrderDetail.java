package com.example.syyam.driverlocation.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShipmentOrderDetail {

    @SerializedName("ConsigneeName")
    @Expose
    private String consigneeName;
    @SerializedName("ShipToName")
    @Expose
    private String shipToName;
    @SerializedName("ShipToAddressOne")
    @Expose
    private String shipToAddressOne;
    @SerializedName("ShipToAddressTwo")
    @Expose
    private String shipToAddressTwo;
    @SerializedName("ShipToStateProvince")
    @Expose
    private String shipToStateProvince;
    @SerializedName("ShipToCity")
    @Expose
    private String shipToCity;
    @SerializedName("CartonQuantity")
    @Expose
    private String cartonQuantity;

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getShipToName() {
        return shipToName;
    }

    public void setShipToName(String shipToName) {
        this.shipToName = shipToName;
    }

    public String getShipToAddressOne() {
        return shipToAddressOne;
    }

    public void setShipToAddressOne(String shipToAddressOne) {
        this.shipToAddressOne = shipToAddressOne;
    }

    public String getShipToAddressTwo() {
        return shipToAddressTwo;
    }

    public void setShipToAddressTwo(String shipToAddressTwo) {
        this.shipToAddressTwo = shipToAddressTwo;
    }

    public String getShipToStateProvince() {
        return shipToStateProvince;
    }

    public void setShipToStateProvince(String shipToStateProvince) {
        this.shipToStateProvince = shipToStateProvince;
    }

    public String getShipToCity() {
        return shipToCity;
    }

    public void setShipToCity(String shipToCity) {
        this.shipToCity = shipToCity;
    }

    public String getCartonQuantity() {
        return cartonQuantity;
    }

    public void setCartonQuantity(String cartonQuantity) {
        this.cartonQuantity = cartonQuantity;
    }

}