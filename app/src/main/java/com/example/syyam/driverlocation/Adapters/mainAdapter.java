package com.example.syyam.driverlocation.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syyam.driverlocation.Models.WaveNumber;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.activities.OpenOrders;


import java.util.Collections;
import java.util.List;

public class mainAdapter extends RecyclerView.Adapter<mainAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    List<WaveNumber> waveNumber= Collections.emptyList();
    OpenOrders activity;

    public mainAdapter(OpenOrders context, List<WaveNumber> waveNumber) {
        inflater=LayoutInflater.from(context);
        this.waveNumber=waveNumber;
        this.activity=context;
    }

    @NonNull


    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comentttt_row, parent, false);
//        View view =inflater.inflate(R.layout.commentttt_row,parent,false);
        MyViewHolder holder=new MyViewHolder(view);
        return holder;
}

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final WaveNumber current= waveNumber.get(position);
        holder.number.setText("Wave Number :"+current.getWaveNumber());
        holder.date.setText("Date: "+current.getWaveDate());

        holder.prLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                activity.method1(current.getWaveNumber());
            }
        });

    }

    @Override
    public int getItemCount() {
        return waveNumber.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        LinearLayout prLinear;
        TextView number;
        TextView date;

        public MyViewHolder(final View itemView) {
            super(itemView);

            number=itemView.findViewById(R.id.number);
            date=itemView.findViewById(R.id.date);
            prLinear=itemView.findViewById(R.id.prlinear);
        }
    }
}
