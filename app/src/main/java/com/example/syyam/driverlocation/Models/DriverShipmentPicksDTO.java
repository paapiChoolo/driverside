package com.example.syyam.driverlocation.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverShipmentPicksDTO {

@SerializedName("ShipmentOrder")
@Expose
private List<ShipmentOrderPicks> shipmentOrder = null;

public List<ShipmentOrderPicks> getShipmentOrder() {
return shipmentOrder;
}

public void setShipmentOrder(List<ShipmentOrderPicks> shipmentOrder) {
this.shipmentOrder = shipmentOrder;
}

}