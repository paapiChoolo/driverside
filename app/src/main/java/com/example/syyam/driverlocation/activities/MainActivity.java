package com.example.syyam.driverlocation.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syyam.driverlocation.AppNotOppenReciever;
import com.example.syyam.driverlocation.Entitity.Locations;
import com.example.syyam.driverlocation.GPS_Service;
import com.example.syyam.driverlocation.MyAppDatabase;
import com.example.syyam.driverlocation.MyAppDatabase_Impl;
import com.example.syyam.driverlocation.R;
import com.facebook.stetho.Stetho;

import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle mToggle;
    private TextView textView;
    private BroadcastReceiver broadcastReceiver;
    private NavigationView mNavigationView;
    private Toolbar mToolbar;

    private Button openWaves;
    private Button pickedWaves;
    private Button delivered;
    private Button cancelled;

    Boolean status;

    private List<Locations> locations;
    public static MyAppDatabase myAppDatabase;
    private String myLocationData;

    public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (broadcastReceiver == null) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
//                    textView.setText("\n" +intent.getExtras().get("coordinates"));


                }
            };
        }
        registerReceiver(broadcastReceiver, new IntentFilter("location_update"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Dashboard");

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);

        Stetho.initializeWithDefaults(this);

        openWaves = (Button) findViewById(R.id.openWaves);
        pickedWaves = (Button) findViewById(R.id.pickedWaves);
        delivered = (Button) findViewById(R.id.delivered);
        cancelled = (Button) findViewById(R.id.Cancelled);

        openWaves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, OpenOrders.class));
                finish();
            }
        });
        pickedWaves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AssignedWaves.class));
                finish();
            }
        });
        delivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, DeliveredActivity.class));
            }
        });
        cancelled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CancelledActivity.class));
            }
        });


        mDrawer = (DrawerLayout) findViewById(R.id.mainID);
        mToggle = new ActionBarDrawerToggle(this, mDrawer, R.string.open, R.string.close);
        mDrawer.addDrawerListener(mToggle);
        mToggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {


                    case R.id.action_myAccount:
                        //startActivity(new Intent(OpenOrders.this, MyAccount.class));
                        break;
                    case R.id.action_logout:
                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("token", null);
                        editor.apply();
                        startActivity(new Intent(MainActivity.this, SplashActivity.class));
                        break;
                    case R.id.action_rate:
                        //rateUS();
                        break;
                    default:
                        return true;
                }

                return true;
            }
        });


        String stat = "";
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        stat = prefs.getString("firstTime", "false");

        if (stat.equals("false")) {
            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putString("firstTime", "true");
            editor.apply();

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.SECOND, 20);
            Log.i("calendar", cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND));

//Create a new PendingIntent and add it to the AlarmManager

            Log.wtf("Alarm", "Al done");
            Intent intent1 = new Intent(getApplicationContext(), AppNotOppenReciever.class);
            intent1.putExtra("id", 1);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1, intent1, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager am = (AlarmManager) getApplicationContext().getSystemService(Activity.ALARM_SERVICE);
            am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        }

        if (!runtime_permissions())
            enable_buttons();

//        locations= myAppDatabase.myDao().getData();
//        if(locations!=null){
//            for (Locations loc: locations){
//                int id=loc.getId();
//                String longitude=loc.getLongitude();
//                String latitude=loc.getLatitude();
//                String battery=loc.getBattery();
//
//                Toast.makeText(MainActivity.this,latitude.toString(),Toast.LENGTH_LONG).show();
//            }
//        }


    }

    private void enable_buttons() {

        Intent i = new Intent(getApplicationContext(), GPS_Service.class);
        startService(i);

    }

    private boolean runtime_permissions() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);

            return true;
        }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                enable_buttons();
            } else {
                runtime_permissions();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId() == R.id.action_logout) {
            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putString("token", null);
            editor.apply();
            startActivity(new Intent(MainActivity.this, SplashActivity.class));
        }
        if (item.getItemId() == R.id.action_myAccount) {
            //startActivity(new Intent(OpenOrders.this, MyAccount.class));
        }
        if (item.getItemId() == R.id.action_rate) {
            //rateUS();
        }
        return super.onOptionsItemSelected(item);
    }
}
