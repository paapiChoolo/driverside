package com.example.syyam.driverlocation.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverShipmentOrderDetailDTO {

@SerializedName("ShipmentOrder")
@Expose
private List<ShipmentOrderDetail> shipmentOrder = null;

public List<ShipmentOrderDetail> getShipmentOrder() {
return shipmentOrder;
}

public void setShipmentOrder(List<ShipmentOrderDetail> shipmentOrder) {
this.shipmentOrder = shipmentOrder;
}

}