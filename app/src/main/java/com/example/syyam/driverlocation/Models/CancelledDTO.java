package com.example.syyam.driverlocation.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CancelledDTO {

@SerializedName("Delivered")
@Expose
private List<Delivered> delivered = null;

public List<Delivered> getDelivered() {
return delivered;
}

public void setDelivered(List<Delivered> delivered) {
this.delivered = delivered;
}

}