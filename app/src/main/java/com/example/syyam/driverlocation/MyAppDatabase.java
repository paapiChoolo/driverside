package com.example.syyam.driverlocation;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.syyam.driverlocation.Entitity.Locations;

@Database(entities = {Locations.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase {
    public abstract MyDao myDao();
}
