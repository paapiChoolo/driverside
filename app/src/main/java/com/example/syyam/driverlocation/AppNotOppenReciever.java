package com.example.syyam.driverlocation;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.arch.persistence.room.Room;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.syyam.driverlocation.Entitity.Locations;
import com.example.syyam.driverlocation.Models.CancelledDTO;
import com.example.syyam.driverlocation.Models.LocationsDTO;
import com.example.syyam.driverlocation.Models.OrderStatusDTO;
import com.example.syyam.driverlocation.Models.StatusData;
import com.example.syyam.driverlocation.Utils.Config;
import com.example.syyam.driverlocation.activities.DriverShipmentOrderActivity;
import com.example.syyam.driverlocation.activities.MainActivity;
import com.example.syyam.driverlocation.activities.confirmCancelActivity;
import com.google.gson.Gson;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rana Bilal on 04/05/18.
 */

public class AppNotOppenReciever extends BroadcastReceiver {

    String TAG = "AppNotOppenReciever";
    public static MyAppDatabase myAppDatabase;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.wtf(TAG, "onReceiveAPPNotOpen: ");
        myAppDatabase = Room.databaseBuilder(context, MyAppDatabase.class, "locations").allowMainThreadQueries().build();

//Trigger the notification
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 50);
        Log.wtf("calendar", cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND));
        final List<Locations> data = myAppDatabase.myDao().getData();

        SendLocation sn=new SendLocation();
        sn.setData(data);
        String dataa = new Gson().toJson(sn);
        Log.wtf("asfasf",dataa);
        if (data != null) {

            Retrofit build = new Retrofit
                    .Builder()
                    .baseUrl(Config.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            API retrofitController = build.create(API.class);
/* Log.wtf("ABC",response.body().toString());
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Log.wtf("asdasd",response.body().getMsg().toString());
                            String asas=response.body().getMsg();
                            for(Locations locations:data)
                            {
                                //delete function
                                myAppDatabase.myDao().delete(locations);
                            }
                        }
                        Log.wtf("asdasd",response.body().getMsg());
                    }*/
            Call<LocationsDTO> wave = retrofitController.DriverLocation(Config.getToken(context),  sn);
            wave.enqueue(new Callback<LocationsDTO>() {
                @Override
                public void onResponse(Call<LocationsDTO> call, Response<LocationsDTO> response) {


                    Log.wtf("ABC",response.body().toString());
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Log.wtf("asdasd",response.body().getMsg());
                            
                            for(Locations locations:data)
                            {
                                //delete function
                                myAppDatabase.myDao().delete(locations);
                            }
                        }
                        Log.wtf("asdasd",response.body().toString());
                    }
                    else
                        Log.wtf("asdasd","PDS");
                }

                @Override
                public void onFailure(Call<LocationsDTO> call, Throwable t) {
                    Log.wtf("asdasd","PDS");
                }
            });

        }

//Create a new PendingIntent and add it to the AlarmManager

        Log.i("Alarm", "Al done");
        Intent intent1 = new Intent(context, AppNotOppenReciever.class);
        intent1.putExtra("id", 1);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent1, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(Activity.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);


    }

}