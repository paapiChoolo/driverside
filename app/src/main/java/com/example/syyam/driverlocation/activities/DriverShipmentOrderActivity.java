package com.example.syyam.driverlocation.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syyam.driverlocation.API;
import com.example.syyam.driverlocation.Adapters.DriverShipmentOrderAdapter;
import com.example.syyam.driverlocation.Models.DriverShipmentOrderDTO;
import com.example.syyam.driverlocation.Models.ShipmentOrder;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.Utils.Config;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DriverShipmentOrderActivity extends AppCompatActivity {

    List<ShipmentOrder> waveNumberr;
    private DriverShipmentOrderAdapter adapter;
    private RecyclerView recyclerView;
    private String data, type;
    private TextView noItems;
    private Button barCode;
    ProgressDialog dialoag;

    @Override
    public void onBackPressed() {
        if (type.equals("Open")) {
            Intent L = new Intent(DriverShipmentOrderActivity.this, OpenOrders.class);
            startActivity(L);
            finish();
        }
        else {
            Intent L = new Intent(DriverShipmentOrderActivity.this, AssignedWaves.class);
            startActivity(L);
            finish();
        }

    }


//    @Override
//    public void onResume()
//    {  // After a pause OR at startup
//        super.onResume();
//        //Refresh your stuff here
//        startActivity(new Intent(DriverShipmentOrderActivity.this,DriverShipmentOrderActivity.class));
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_shipment_order);
        setTitle("Driver Shipment Order");

        Intent iin = getIntent();
        Bundle b = iin.getExtras();

        if (b != null) {
            data = (String) b.get("data");  // WaveNumber
            type = (String) b.get("type"); // what activity is calling current screen, is store in type. (Assign or Open)

//            if(type.equals("AssignedWaves")){
//                barCode.setVisibility(View.GONE);
//            }
        }

        dialoag = new ProgressDialog(DriverShipmentOrderActivity.this);
        dialoag.setMessage("Loading...");
        dialoag.setCanceledOnTouchOutside(false);
        dialoag.show();

        noItems = (TextView) findViewById(R.id.noItems);
        noItems.setVisibility(View.GONE);

        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        getData(data);
        adapter = new DriverShipmentOrderAdapter(DriverShipmentOrderActivity.this, waveNumberr);
    }

    public void method1(String orderNumber) {
        if (type.equals("Assign")) {
            Intent n = new Intent(DriverShipmentOrderActivity.this, DriverShipmentOrderDetailActivity.class);
            n.putExtra("data", orderNumber);
            n.putExtra("waveNumber", data);
            n.putExtra("type", "Assign");
            startActivity(n);
            finish();
        } else {
            Intent n = new Intent(DriverShipmentOrderActivity.this, DriverShipmentOrderPicks.class);
            n.putExtra("waveNumber", data);
            n.putExtra("orderNumber", orderNumber);
            n.putExtra("type", "Open");
            startActivity(n);
            finish();

        }

    }
    public void method2() {
        noItems.setVisibility(View.VISIBLE);
            finish();
    }
    private void getData(final String data) {
        Retrofit build = new Retrofit
                .Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        API retrofitController = build.create(API.class);

        Call<DriverShipmentOrderDTO> wave = retrofitController.DriverShipmentOrder(Config.getToken(DriverShipmentOrderActivity.this), data, type);
        wave.enqueue(new Callback<DriverShipmentOrderDTO>() {
            @Override
            public void onResponse(Call<DriverShipmentOrderDTO> call, Response<DriverShipmentOrderDTO> response) {
                dialoag.dismiss();
                if (response.isSuccessful()) {
                    if (!response.body().getShipmentOrder().isEmpty()) {

                        waveNumberr = response.body().getShipmentOrder();
                        DriverShipmentOrderAdapter adapter = new DriverShipmentOrderAdapter(DriverShipmentOrderActivity.this, waveNumberr);
                        recyclerView.setAdapter(adapter);
                    } else {
                        noItems.setVisibility(View.VISIBLE);
//                        if (type.equals("Open")) {
//                            Intent L = new Intent(DriverShipmentOrderActivity.this, OpenOrders.class);
//                            startActivity(L);
//                            finish();
//                        }
//                        else if(type.equals("Assign")) {
//
//                            Intent L = new Intent(DriverShipmentOrderActivity.this, AssignedWaves.class);
//                            startActivity(L);
//                            finish();
//                        }
                    }

                }
                else{
                    noItems.setVisibility(View.VISIBLE);
                    if (type.equals("Open")) {
                        Intent L = new Intent(DriverShipmentOrderActivity.this, OpenOrders.class);
                        startActivity(L);
                        finish();
                    }
                    else if(type.equals("Assign")) {
                        Intent L = new Intent(DriverShipmentOrderActivity.this, AssignedWaves.class);
                        startActivity(L);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<DriverShipmentOrderDTO> call, Throwable t) {
                noItems.setVisibility(View.VISIBLE);
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_home) {
            Intent back = new Intent(DriverShipmentOrderActivity.this, MainActivity.class);
            startActivity(back);
            finish();
        }
        if (item.getItemId()==R.id.action_back)
        {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
