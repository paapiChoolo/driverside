package com.example.syyam.driverlocation.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syyam.driverlocation.API;
import com.example.syyam.driverlocation.Adapters.AssignedWavesAdapter;
import com.example.syyam.driverlocation.Models.DriverShipmentOrderDetailDTO;
import com.example.syyam.driverlocation.Models.OrderStatusDTO;
import com.example.syyam.driverlocation.Models.RawTokenBody;
import com.example.syyam.driverlocation.Models.ScanOrderDTO;
import com.example.syyam.driverlocation.Models.StatusData;
import com.example.syyam.driverlocation.Models.WavesDTO;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.Utils.Config;
import com.rengwuxian.materialedittext.MaterialEditText;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DriverShipmentOrderDetailActivity extends AppCompatActivity {

    private String data, waveNumber;

    private TextView ConsigneeName;
    private TextView ShipToName;
    private TextView ShipToAddressOne;
    private TextView ShipToAddressTwo;
    private TextView ShipToStateProvince;
    private TextView ShipToCity;
    private TextView CartonQuantity;

    Bitmap bitmap;

    RelativeLayout rootLayout;

    private Button Delivered;
    private Button Cancelled;

    public void onBackPressed() {
        Intent L = new Intent(DriverShipmentOrderDetailActivity.this, DriverShipmentOrderActivity.class);
        L.putExtra("data",waveNumber);
        L.putExtra("type","Assign");
        startActivity(L);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_shipment_order_detail);
        setTitle("Driver Shipment Order Detail");

        Intent iin = getIntent();
        Bundle b = iin.getExtras();

        if (b != null) {
            data = (String) b.get("data");
            waveNumber=(String) b.get("waveNumber");
            getData(data);
        }

        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);

        ConsigneeName = (TextView) findViewById(R.id.ConsigneeName);
        ShipToName = (TextView) findViewById(R.id.ShipToName);
        ShipToAddressOne = (TextView) findViewById(R.id.ShipToAddressOne);
        ShipToAddressTwo = (TextView) findViewById(R.id.ShipToAddressTwo);
        ShipToStateProvince = (TextView) findViewById(R.id.ShipToStateProvince);
        ShipToCity = (TextView) findViewById(R.id.ShipToCity);
        CartonQuantity = (TextView) findViewById(R.id.CartonQuantity);

        Delivered = (Button) findViewById(R.id.Delivered);
        Cancelled = (Button) findViewById(R.id.Cancelled);

        Cancelled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent i=new Intent(DriverShipmentOrderDetailActivity.this,confirmCancelActivity.class);
                    i.putExtra("orderNumber",data);
                    i.putExtra("waveNumber",waveNumber);
                    i.putExtra("type", "cancel");
                    startActivity(i);
                    finish();

            }
        });
        Delivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(DriverShipmentOrderDetailActivity.this,confirmCancelActivity.class);
                i.putExtra("orderNumber",data);
                i.putExtra("waveNumber",waveNumber);
                i.putExtra("type", "deliver");
                startActivity(i);
                finish();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        bitmap=(Bitmap)data.getExtras().get("data");

    }

//    private void showCancelDialog(final String OrderNumber) {
//        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
//        dialog.setTitle("Cancel Order ");
//        dialog.setMessage("Please Enter Reason for Cancellation");
//
//        LayoutInflater inflater = LayoutInflater.from(this);
//        View login_layout = inflater.inflate(R.layout.order_status, null);
//
//        final MaterialEditText reason = login_layout.findViewById(R.id.reason);
//        final LinearLayout cameraBtn=  login_layout.findViewById(R.id.cameraBtn);
//        final ImageView imageView=  login_layout.findViewById(R.id.imageView);
//
//        dialog.setView(login_layout);
//
//
//        cameraBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(intent,0);
//                imageView.setImageBitmap(bitmap);
//            }
//        });
//
//
//
//        //Set Buttons
//        dialog.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int i) {
//                dialog.dismiss();
//
//                //Diable button if processing
////                Cancelled.setEnabled(false);
//
//                // Check validation
//

//                String Reason=reason.getText().toString();
//                if (TextUtils.isEmpty(Reason)) {
//                    final Snackbar snackbar = Snackbar.make(rootLayout, "Please Enter A Reason", Snackbar.LENGTH_LONG);
//                    snackbar.setAction("DISMISS", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            snackbar.dismiss();
//                        }
//                    });
//                    snackbar.show();
//
//                    return;
//                }
//                if (Reason.length() < 6) {
//                    final Snackbar snackbar = Snackbar.make(rootLayout, "Reason Is Not Long Enough!", Snackbar.LENGTH_LONG);
//                    snackbar.setAction("DISMISS", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            snackbar.dismiss();
//                        }
//                    });
//                    snackbar.show();
//                    return;
//                }
//
//                //Loader
//                final SpotsDialog waitingDialog = new SpotsDialog(DriverShipmentOrderDetailActivity.this);
//                waitingDialog.show();
//
//                //...
//
//                Retrofit build = new Retrofit
//                        .Builder()
//                        .baseUrl(Config.BASE_URL)
//                        .addConverterFactory(GsonConverterFactory.create())
//                        .build();
//                API retrofitController = build.create(API.class);
//
//                StatusData statusData=new StatusData();
//                statusData.setOrderNumber(OrderNumber);
//                statusData.setReason(Reason);
//                statusData.setStatus("Cancelled");
//
//
//                Call<OrderStatusDTO> wave = retrofitController.DriverShipmentOrderStatus(Config.getToken(DriverShipmentOrderDetailActivity.this), statusData);
//                wave.enqueue(new Callback<OrderStatusDTO>() {
//                    @Override
//                    public void onResponse(Call<OrderStatusDTO> call, Response<OrderStatusDTO> response) {
//                        if(response.isSuccessful())
//                        {
//                            waitingDialog.dismiss();
//                            if (response.body()!=null)
//                            {
//                                Toast.makeText(DriverShipmentOrderDetailActivity.this,response.body().getMsg(),Toast.LENGTH_LONG).show();
//                                Log.wtf("1mint:",response.body().getMsg() );
//                            }
//
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<OrderStatusDTO> call, Throwable t) {
//                        waitingDialog.dismiss();
//                    }
//                });
//            }
//        });
//        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//
//
//        dialog.show();
//
//    }

    public void getData(String dataa) {

        Retrofit build = new Retrofit
                .Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        API retrofitController = build.create(API.class);

        final Call<DriverShipmentOrderDetailDTO> wave = retrofitController.DriverShipmentOrderDetail(Config.getToken(DriverShipmentOrderDetailActivity.this), dataa, waveNumber);
        wave.enqueue(new Callback<DriverShipmentOrderDetailDTO>() {
            @Override
            public void onResponse(Call<DriverShipmentOrderDetailDTO> call, Response<DriverShipmentOrderDetailDTO> response) {
                DriverShipmentOrderDetailDTO dto = response.body();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (!response.body().getShipmentOrder().isEmpty()) {

                            ConsigneeName.setText(response.body().getShipmentOrder().get(0).getConsigneeName());
                            ShipToName.setText(response.body().getShipmentOrder().get(0).getShipToName());
                            ShipToAddressOne.setText(response.body().getShipmentOrder().get(0).getShipToAddressOne());
                            ShipToAddressTwo.setText(response.body().getShipmentOrder().get(0).getShipToAddressTwo());
                            ShipToStateProvince.setText(response.body().getShipmentOrder().get(0).getShipToStateProvince());
                            ShipToCity.setText(response.body().getShipmentOrder().get(0).getShipToCity());
                            CartonQuantity.setText(response.body().getShipmentOrder().get(0).getCartonQuantity());
                        }
                        else {
//                            Toast.makeText(DriverShipmentOrderDetailActivity.this,"No Order Detail To Show.",Toast.LENGTH_LONG).show();
                            Intent L = new Intent(DriverShipmentOrderDetailActivity.this, DriverShipmentOrderActivity.class);
                            L.putExtra("data",waveNumber);
                            L.putExtra("type","Assign");
                            startActivity(L);
                            finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<DriverShipmentOrderDetailDTO> call, Throwable t) {

            }


        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==R.id.action_home)
        {
            Intent back=new Intent(DriverShipmentOrderDetailActivity.this,MainActivity.class);
            startActivity(back);
            finish();
        }
        if (item.getItemId()==R.id.action_back)
        {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
