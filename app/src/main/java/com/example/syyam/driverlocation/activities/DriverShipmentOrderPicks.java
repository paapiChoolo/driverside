package com.example.syyam.driverlocation.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.syyam.driverlocation.API;
import com.example.syyam.driverlocation.Adapters.DriverShipmentOrderAdapter;
import com.example.syyam.driverlocation.Adapters.DriverShipmentOrderPicksAdapter;
import com.example.syyam.driverlocation.Models.DriverShipmentOrderDTO;
import com.example.syyam.driverlocation.Models.DriverShipmentPicksDTO;
import com.example.syyam.driverlocation.Models.ShipmentOrderPicks;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.Utils.Config;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DriverShipmentOrderPicks extends AppCompatActivity {

    List<ShipmentOrderPicks> waveNumberr;
    private DriverShipmentOrderPicksAdapter adapter;

    private RecyclerView recyclerView;
    private String data, type, wave;
    private Button barCode;

    ProgressDialog dialoag;

    @Override
    public void onBackPressed() {
        Intent L = new Intent(DriverShipmentOrderPicks.this, DriverShipmentOrderActivity.class);
        L.putExtra("data", wave);
        L.putExtra("type", "Open");
        startActivity(L);
        finish();
    }


//    @Override
//    public void onResume()
//    {  // After a pause OR at startup
//        super.onResume();
//        //Refresh your stuff here
//        startActivity(new Intent(DriverShipmentOrderPicks.this,DriverShipmentOrderPicks.class));
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_shipment_order_picks);
        setTitle("Driver Shipment Order Picks");

        dialoag = new ProgressDialog(DriverShipmentOrderPicks.this);
        dialoag.setMessage("Loading...");
        dialoag.setCanceledOnTouchOutside(false);
        dialoag.show();

        barCode = (Button) findViewById(R.id.barCode);
        barCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(DriverShipmentOrderPicks.this, BarCodeActivity.class);
                n.putExtra("orderNumber", data);
                n.putExtra("waveNumber", wave);
                startActivity(n);
                finish();
            }
        });


        Intent iin = getIntent();
        Bundle b = iin.getExtras();

        if (b != null) {
            data = (String) b.get("orderNumber");  // OrderNumber
            wave = (String) b.get("waveNumber");
            type = (String) b.get("type"); //Open or Assign
        }
        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        getData(data, wave);
        adapter = new DriverShipmentOrderPicksAdapter(DriverShipmentOrderPicks.this, waveNumberr);

    }

//    protected void onActivityResult(int requestCode, int resultCode, Intent d) {
//
//        if (requestCode == 1) {
//
//            if (resultCode == RESULT_OK) {
//                //Update List
//                startActivity(new Intent(DriverShipmentOrderPicks.this,DriverShipmentOrderPicks.class));
//            }
//            if (resultCode == RESULT_CANCELED) {
//                //Do nothing?
//            }
//        }
//    }//onActivityResult

    private void getData(final String data, final String wave) {
        Retrofit build = new Retrofit
                .Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        API retrofitController = build.create(API.class);

        Call<DriverShipmentPicksDTO> wavee = retrofitController.DriverShipmentPicks(Config.getToken(DriverShipmentOrderPicks.this), data, wave);
        wavee.enqueue(new Callback<DriverShipmentPicksDTO>() {
            @Override
            public void onResponse(Call<DriverShipmentPicksDTO> call, Response<DriverShipmentPicksDTO> response) {
                dialoag.dismiss();
                if (response.isSuccessful()) {
                    if (!response.body().getShipmentOrder().isEmpty()) {
                        waveNumberr = response.body().getShipmentOrder();
                        DriverShipmentOrderPicksAdapter adapter = new DriverShipmentOrderPicksAdapter(DriverShipmentOrderPicks.this, waveNumberr);
                        recyclerView.setAdapter(adapter);
                    } else {
                        Toast.makeText(DriverShipmentOrderPicks.this, "Open The Top Order First.", Toast.LENGTH_LONG).show();
                        Intent L = new Intent(DriverShipmentOrderPicks.this, DriverShipmentOrderActivity.class);
                        L.putExtra("data", wave);
                        L.putExtra("type", type);
                        startActivity(L);
                        finish();
                    }

                } else {
                    Intent L = new Intent(DriverShipmentOrderPicks.this, DriverShipmentOrderActivity.class);
                    L.putExtra("data", wave);
                    L.putExtra("type", type);
                    startActivity(L);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<DriverShipmentPicksDTO> call, Throwable t) {

            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_home) {
            Intent back = new Intent(DriverShipmentOrderPicks.this, MainActivity.class);
            startActivity(back);
            finish();
        }
        if (item.getItemId()==R.id.action_back)
        {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
