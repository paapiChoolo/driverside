package com.example.syyam.driverlocation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.text.format.DateUtils;


import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


/**
 * Created by Spartan_Base on 9/23/2017.
 */

public class ApplicationUtils {

    public static int primaryDarkColor, appBarColor = Color.parseColor("#0054a6"), backgroundColor, textColor, transparentColor, primaryTextColor, secondaryTextColor, listItemBgColor, color1, color2, color3;
    public static String primaryDarkColorString;

    public static List<String> getOptions() {
        List<String> options = new ArrayList<>();


        options.add("All");
        options.add("Pending");
        options.add("Missed");
        options.add("Cancelled");
        options.add("Completed");
        return options;
    }


    public static boolean isEmptyString(String text) {
        return (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0);
    }


    public static Calendar getReminderTimeForBooking(String date, String startTime) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH).parse(date + " " + startTime));
            int hour = calendar.get(Calendar.HOUR_OF_DAY) - 4;
            calendar.set(Calendar.HOUR_OF_DAY, hour);
//            calendar.setTime(new Date());
//            calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE)+1);
            return calendar;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void inviteFreinds(Activity activity) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Body Here");
        activity.startActivity(Intent.createChooser(sharingIntent, "Invite Freinds to Retainoo"));
    }

    public final static boolean isValidEmail(String target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static int pxFromDp(final Context context, final float dp) {
        return (int) Math.ceil(dp * context.getResources().getDisplayMetrics().density);

    }

    public static String getDesignation(String des) {
        switch (des) {
            case "JUNIOR_STYLIST":
                return "Junior Stylist";

            case "SENIOR_STYLIST":
                return "Senior Stylist";
            case "JUNIOR_HAIR_STYLIST":
                return "Junior Hair Stylist";

            case "SENIOR_HAIR_STYLIST":
                return "Senior Hair Stylist";

            case "SALON_MANAGER":
                return "Manager & Senior Stylist";
            case "SALON_OWNER":
                return "Owner & Senior Stylist";
            /*case "SALON_DIRECTOR":
                return "Salon Director";*/
            default:
                return "Stylist";

        }
    }

    public static String getDesignationEnum(String des) {
        switch (des) {
            case "Junior Stylist":
                return "JUNIOR_STYLIST";

            case "Senior Stylist":
                return "SENIOR_STYLIST";
            case "Junior Hair Stylist":
                return "JUNIOR_HAIR_STYLIST";

            case "Senior Hair Stylist":
                return "SENIOR_HAIR_STYLIST";

            case "Manager & Senior Stylist":
                return "SALON_MANAGER";
            case "Owner & Senior Stylist":
                return "SALON_OWNER";
            /*case "SALON_DIRECTOR":
                return "Salon Director";*/
            default:
                return "Stylist";

        }
    }

    public static void setAppColors(String theme) {
        try {
            JSONObject jsonObject = new JSONObject(theme);
            primaryDarkColor = Color.parseColor("#" + jsonObject.getString("statusbarColor"));
            backgroundColor = Color.parseColor("#" + jsonObject.getString("backgroundColor"));
            appBarColor = Color.parseColor("#" + jsonObject.getString("appbarColor"));
            textColor = Color.parseColor("#" + jsonObject.getString("textColor"));
            transparentColor = Color.parseColor("#1A" + jsonObject.getString("appbarColor"));

        } catch (JSONException e) {
            primaryDarkColor = Color.parseColor("#" + "F44336");
            backgroundColor = Color.parseColor("#" + "ffffff");
            appBarColor = Color.parseColor("#" + "F44336");
            textColor = Color.parseColor("#" + "000000");
            transparentColor = Color.parseColor("#1AF44336");

            e.printStackTrace();
        } catch (Exception e) {
            primaryDarkColor = Color.parseColor("#" + "F44336");
            backgroundColor = Color.parseColor("#" + "ffffff");
            appBarColor = Color.parseColor("#" + "F44336");
            textColor = Color.parseColor("#" + "000000");
            transparentColor = Color.parseColor("#1AF44336");
        }
    }

    private static final int SHORT_DATE_FLAGS = DateUtils.FORMAT_SHOW_DATE
            | DateUtils.FORMAT_NO_YEAR | DateUtils.FORMAT_ABBREV_ALL;
    private static final int FULL_DATE_FLAGS = DateUtils.FORMAT_SHOW_TIME
            | DateUtils.FORMAT_ABBREV_ALL | DateUtils.FORMAT_SHOW_DATE;


    private static boolean today(Date date) {
        return sameDay(date, new Date(System.currentTimeMillis()));
    }


    private static boolean sameDay(Date a, Date b) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(a);
        cal2.setTime(b);
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.DAY_OF_YEAR) == cal2
                .get(Calendar.DAY_OF_YEAR);
    }

    public static String getImagePathForS3(String businessName) {
        return java.util.UUID.randomUUID().toString() + ".jpg"; //"/"+businessName+"/news/"+
    }

    public static Date getDateTime(String date) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(date);

    }

    public static String convertDateTime(String date) throws ParseException {
        Date simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(date);
        java.text.DateFormat destDf = new SimpleDateFormat("HH:mm  EE, MMMM dd, yyyy");
        String time = destDf.format(simpleDateFormat);
        System.out.println("Converted date is : " + time);
        return time;
    }

    public static String convertDateTimeUtcToLocal(String date) throws ParseException {
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        simpleDate.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date simpleDateFormat = simpleDate.parse(date);


        java.text.DateFormat destDf = new SimpleDateFormat("HH:mm  EE, MMMM dd, yyyy");
        destDf.setTimeZone(TimeZone.getDefault());
        String time = destDf.format(simpleDateFormat);
        System.out.println("Converted date is : " + time);
        return time;
    }

    public static String convertDateTimeLocalToUTC(String date) {
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        simpleDate.setTimeZone(TimeZone.getDefault());
        Date simpleDateFormat = null;
        try {
            simpleDateFormat = simpleDate.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        java.text.DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        destDf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String time = destDf.format(simpleDateFormat);
        System.out.println("Converted date is : " + time);
        return time;
    }

    public static String convertDateLocalToUtc(String date) {
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
        simpleDate.setTimeZone(TimeZone.getDefault());
        Date simpleDateFormat = null;
        try {
            simpleDateFormat = simpleDate.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        java.text.DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd");
        destDf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String time = destDf.format(simpleDateFormat);
        System.out.println("Converted date is : " + time);
        return time;
    }


    public static String convertDateUtcToLocal(String date) {
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
        simpleDate.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date simpleDateFormat = null;
        try {
            simpleDateFormat = simpleDate.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        java.text.DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd");
        destDf.setTimeZone(TimeZone.getDefault());
        String time = destDf.format(simpleDateFormat);
        System.out.println("Converted date is : " + time);
        return time;
    }

    public static String convertTimeLocalToUtc(String date) {
        SimpleDateFormat simpleDate = new SimpleDateFormat("HH:mm:ss");
        simpleDate.setTimeZone(TimeZone.getDefault());
        Date simpleDateFormat = null;
        try {
            simpleDateFormat = simpleDate.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        SimpleDateFormat destDf = new SimpleDateFormat("HH:mm:ss");
        destDf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String time = destDf.format(simpleDateFormat);
        System.out.println("Converted date is : " + time);
        return time;
    }


    public static String convertTimeUtcToLocal(String date) {
        SimpleDateFormat simpleDate = new SimpleDateFormat("HH:mm:ss");
        simpleDate.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date simpleDateFormat = null;
        try {
            simpleDateFormat = simpleDate.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        java.text.DateFormat destDf = new SimpleDateFormat("HH:mm:ss");
        destDf.setTimeZone(TimeZone.getDefault());
        String time = destDf.format(simpleDateFormat);
        System.out.println("Converted date is : " + time);
        return time;
    }


    public static boolean isTimeAfter(String timeSlot, Time time) {

        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(getDateTime(timeSlot + "T" + time.toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

// Set time of calendar to 18:00


        return cal.after(Calendar.getInstance());
    }

    public static Date returnDate(String sDate1) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static boolean isGreaterDate(String sDate1, String sDate2) {
        Date date1 = null, date2 = null;
        try {
            date1 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate1);
            date2 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date1.after(date2) || date1.equals(date2)) {
            return false;

        } else {
            return true;
        }
    }


    public static String convertTime(String time) {
        try {
            String[] t = time.split(":");
            return t[0] + ":" + t[1];
        } catch (Exception e) {
            return "";
        }
    }

    public static String convertTimeFromMilis(long time) {

        Date date = new Date(time);
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        String dateFormatted = formatter.format(date);
        return dateFormatted;
    }

    /*
     * Gets the file path of the given Uri.
     */
    @SuppressLint("NewApi")
    public static String getPath(Uri uri, Context context) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static String convertDate(String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        java.text.DateFormat srcDf = formatter;

        try {
            Date date = srcDf.parse(dateString);
            dateString = convertDate(date);
            return dateString;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public static String convertDateLongName(String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        java.text.DateFormat srcDf = formatter;

        try {
            Date date = srcDf.parse(dateString);
            dateString = convertDateLongName(date);
            return dateString;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public static String convertDate(Date date) {
        SimpleDateFormat destDf = new SimpleDateFormat("EE - dd MMMM, yyyy");
        String dateString = destDf.format(date);
        System.out.println("Converted date is : " + dateString);
        return dateString;
    }

    public static String convertDateLongName(Date date) {
        SimpleDateFormat destDf = new SimpleDateFormat("EEEE - dd MMMM, yyyy");
        String dateString = destDf.format(date);
        System.out.println("Converted date is : " + dateString);
        return dateString;
    }

    public static String convertDateStamp(Date date) {
        SimpleDateFormat destDf = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = destDf.format(date);
        System.out.println("Converted date is : " + dateString);
        return dateString;
    }

    public static String getToday() {
        return convertDateLongName(new Date());
    }

    public static String fetchDayFromDate(String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        java.text.DateFormat srcDf = formatter;
        Date date = null;


        try {
            date = srcDf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        java.text.DateFormat destDf = new SimpleDateFormat("EEEE");
        dateString = destDf.format(date);
        System.out.println("Converted date is : " + dateString);
        return dateString.toUpperCase();
    }

    public static String getTodayDay() {
        Date date = new Date();


        java.text.DateFormat destDf = new SimpleDateFormat("EEEE");
        String dateString = destDf.format(date);
        System.out.println("Converted date is : " + dateString);
        return dateString.toUpperCase();
    }

    public static String convertTimeAM(String time) {


        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        java.text.DateFormat srcDf = formatter;
        Date date = null;


        try {
            date = srcDf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        java.text.DateFormat destDf = new SimpleDateFormat("hh:mm a");
        time = destDf.format(date);
        System.out.println("Converted date is : " + time);
        return time;
    }





    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



}