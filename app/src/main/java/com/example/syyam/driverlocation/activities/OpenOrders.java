package com.example.syyam.driverlocation.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.syyam.driverlocation.API;
import com.example.syyam.driverlocation.Adapters.mainAdapter;
import com.example.syyam.driverlocation.GPS_Service;
import com.example.syyam.driverlocation.Models.RawTokenBody;
import com.example.syyam.driverlocation.Models.WaveNumber;
import com.example.syyam.driverlocation.Models.WavesDTO;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.Utils.Config;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OpenOrders extends AppCompatActivity {
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle mToggle;
    private TextView noItems;

    private NavigationView mNavigationView;
    private Toolbar mToolbar;
    List<WaveNumber> waveNumber;
    private LinearLayoutManager mLayoutManager;

    private mainAdapter adapter;
    private RecyclerView recyclerView;

    ProgressDialog dialoag;

    public static final String MY_PREFS_NAME = "MyPrefsFile";



    @Override
    public void onBackPressed() {
        Intent L = new Intent(OpenOrders.this, MainActivity.class);
        startActivity(L);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_order);
        setTitle("Open Waves");

        noItems=(TextView) findViewById(R.id.noItems);
        noItems.setVisibility(View.GONE);

        dialoag = new ProgressDialog(OpenOrders.this);
        dialoag.setMessage("Loading Data");
        dialoag.setCanceledOnTouchOutside(false);
        dialoag.show();

        recyclerView= findViewById(R.id.recyclerView);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        getData();
        adapter=new mainAdapter(OpenOrders.this, waveNumber);
//        recyclerView.setAdapter(adapter);
//        recyclerView.setLayoutManager(new LinearLayoutManager(OpenOrders.this));

    }

    private void getData() {

        Retrofit build = new Retrofit
                .Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API retrofitController = build.create(API.class);
        RawTokenBody rawTokenBody=new RawTokenBody();
        rawTokenBody.setToken(Config.getToken(OpenOrders.this));
        Call<WavesDTO> wave = retrofitController.DriverWaves(Config.getToken(OpenOrders.this),rawTokenBody);
        wave.enqueue(new Callback<WavesDTO>() {
            @Override
            public void onResponse(Call<WavesDTO> call, Response<WavesDTO> response) {
                dialoag.dismiss();
                if(response.isSuccessful())
                {
                    if (!response.body().getWaveNumber().isEmpty())
                    {
                        waveNumber = response.body().getWaveNumber();
                        mainAdapter adapter=new mainAdapter(OpenOrders.this, waveNumber);
                        recyclerView.setAdapter(adapter);
                    }
                    else
                        noItems.setVisibility(View.VISIBLE);

                }
                else
                {
                    noItems.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<WavesDTO> call, Throwable t) {
                noItems.setVisibility(View.VISIBLE);
            }
        });

    }



    public void method1(String waveNumber){
        Intent n=new Intent(OpenOrders.this, DriverShipmentOrderActivity.class);
        n.putExtra("type","Open");
        n.putExtra("data",waveNumber);
        startActivity(n);
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.openassign_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==R.id.action_back)
        {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}