package com.example.syyam.driverlocation.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.syyam.driverlocation.API;
import com.example.syyam.driverlocation.Adapters.AssignedWavesAdapter;
import com.example.syyam.driverlocation.Adapters.CancelledAdapter;
import com.example.syyam.driverlocation.Models.CancelledDTO;
import com.example.syyam.driverlocation.Models.Delivered;
import com.example.syyam.driverlocation.Models.WaveNumber;
import com.example.syyam.driverlocation.Models.WavesDTO;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.Utils.Config;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CancelledActivity extends AppCompatActivity {

    List<Delivered> delivered;
    private LinearLayoutManager mLayoutManager;

    ProgressDialog dialoag;

    private CancelledAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancelled);
        setTitle("Cancelled Waves");

        dialoag = new ProgressDialog(CancelledActivity.this);
        dialoag.setMessage("Loading Data");
        dialoag.setCanceledOnTouchOutside(false);
        dialoag.show();

        recyclerView= findViewById(R.id.recyclerViewAssigned);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        getData();
        adapter=new CancelledAdapter(CancelledActivity.this, delivered);

    }
//    public void method1(String waveNumber){
//        Intent n=new Intent(CancelledActivity.this, DriverShipmentOrderActivity.class);
//        n.putExtra("data",waveNumber);
//        startActivity(n);
//
//    }
    private void getData() {
        Retrofit build = new Retrofit
                .Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        API retrofitController = build.create(API.class);

        Call<CancelledDTO> wave = retrofitController.DriverCancelOrders(Config.getToken(CancelledActivity.this));
        wave.enqueue(new Callback<CancelledDTO>() {
            @Override
            public void onResponse(Call<CancelledDTO> call, Response<CancelledDTO> response) {
                dialoag.dismiss();
                if(response.isSuccessful())
                {

                    if (response.body().getDelivered()!=null)
                    {
                        delivered = response.body().getDelivered();
                        CancelledAdapter adapter=new CancelledAdapter(CancelledActivity.this, delivered);

                        recyclerView.setAdapter(adapter);
                    }

                }
            }

            @Override
            public void onFailure(Call<CancelledDTO> call, Throwable t) {

            }
        });

    }
}
