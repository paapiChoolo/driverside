package com.example.syyam.driverlocation.Models;

public class LoginData {
    private String name;
    private String password;

    public String getUserName() {
        return name;
    }

    public void setUserName(String userName) {
        name = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String pass) {
        password = pass;
    }
}
