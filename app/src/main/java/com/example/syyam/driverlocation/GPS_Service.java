package com.example.syyam.driverlocation;

import android.annotation.SuppressLint;
import android.app.Service;
import android.arch.persistence.room.Room;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.example.syyam.driverlocation.Entitity.Locations;
import com.example.syyam.driverlocation.activities.MainActivity;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by filipp on 6/16/2016.
 */
public class GPS_Service extends Service {

    private LocationListener listener;
    private LocationManager locationManager;

    private List<Locations> locations;
    public static MyAppDatabase myAppDatabase;
    Date currentTime;

    String bat;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            bat=String.valueOf(level) + "%";
        }
    };
    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        super.onCreate();
        Log.wtf("TAG", "Service created.");
    }

    @SuppressLint("MissingPermission")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.wtf("TAG", "Service started.");

        currentTime = Calendar.getInstance().getTime();
        myAppDatabase= Room.databaseBuilder(getApplicationContext(),MyAppDatabase.class,"locations").allowMainThreadQueries().build();

        this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.wtf("hae"," "+ location.getLongitude()+" "+location.getLatitude()+ " " + bat );

                String longitude=location.getLongitude()+"";
                String latitude=location.getLatitude()+"";
                String battery=bat+"";
                String time=currentTime+"";

                Locations L=new Locations();
                L.setLongitude(longitude);
                L.setLatitude(latitude);
                L.setBattery(battery);
                L.setTime(time);

                myAppDatabase.myDao().addLocations(L);

                Intent i = new Intent("location_update");
                i.putExtra("coordinates",location.getLongitude()+" "+location.getLatitude()+ " " + bat);
                sendBroadcast(i);

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };

        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        //noinspection MissingPermission
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,360000,0,listener);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(locationManager != null){
            //noinspection MissingPermission
            locationManager.removeUpdates(listener);
        }
    }
}