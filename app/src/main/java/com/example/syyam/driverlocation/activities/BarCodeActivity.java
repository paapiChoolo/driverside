package com.example.syyam.driverlocation.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.syyam.driverlocation.API;
import com.example.syyam.driverlocation.Adapters.DriverShipmentOrderAdapter;
import com.example.syyam.driverlocation.Models.LoginDTO;
import com.example.syyam.driverlocation.Models.ScanOrderDTO;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.Utils.Config;
import com.google.zxing.Result;

import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.Manifest.permission.CAMERA;

public class BarCodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private static final int REQUEST_CAMERA = 1;
    private ZXingScannerView scannerView;
    private static int camId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private String myResult;
    ProgressDialog dialoag;

    private String data, wave;
    List<ScanOrderDTO> scanOrderdto;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
        int currentApiVersion = Build.VERSION.SDK_INT;


        if (currentApiVersion >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
//                Toast.makeText(getApplicationContext(), "Permission already granted!", Toast.LENGTH_LONG).show();
            } else {
                requestPermission();
            }
        }

        Intent iin = getIntent();
        Bundle b = iin.getExtras();

        if (b != null) {
            data = (String) b.get("orderNumber");
            wave = (String) b.get("waveNumber");
        }

    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    @Override
    public void onResume() {
        super.onResume();

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (scannerView == null) {
                    scannerView = new ZXingScannerView(this);
                    setContentView(scannerView);
                }
                scannerView.setResultHandler(this);
                scannerView.startCamera();
            } else {
                requestPermission();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        scannerView.stopCamera();
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted) {
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access camera", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access and camera", Toast.LENGTH_LONG).show();
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (shouldShowRequestPermissionRationale(CAMERA)) {
                                    showMessageOKCancel("You need to allow access to both the permissions",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                        requestPermissions(new String[]{CAMERA},
                                                                REQUEST_CAMERA);
                                                    }
                                                }
                                            });
                                    return;
                                }
                            }
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent n = new Intent(BarCodeActivity.this, DriverShipmentOrderPicks.class);
        n.putExtra("orderNumber", data);
        n.putExtra("waveNumber", wave);
        startActivity(n);
        finish();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(BarCodeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public void getData(final String orderNum, final String waveNum) {

//        Log.d("QRCodeScanner", result.getText());
//        Log.d("QRCodeScanner", result.getBarcodeFormat().toString());

        Retrofit build = new Retrofit
                .Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        API retrofitController = build.create(API.class);

        final Call<ScanOrderDTO> wave = retrofitController.DriverScanOrder(Config.getToken(BarCodeActivity.this), waveNum, myResult, orderNum);
        wave.enqueue(new Callback<ScanOrderDTO>() {
            @Override
            public void onResponse(Call<ScanOrderDTO> call, Response<ScanOrderDTO> response) {
                ScanOrderDTO dto = response.body();
                Toast.makeText(BarCodeActivity.this, response.body().getMsg(), Toast.LENGTH_LONG).show();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        dialoag.dismiss();
                        Intent L = new Intent(BarCodeActivity.this, DriverShipmentOrderPicks.class);
                        L.putExtra("orderNumber", orderNum);
                        L.putExtra("waveNumber", waveNum);
                        L.putExtra("type", "Open");
                        startActivity(L);
                        finish();
//                        Intent returnIntent = new Intent();
//                        setResult(RESULT_CANCELED, returnIntent);
//                        finish();
                    } else {
                        dialoag.dismiss();
                        finish();
                    }
                } else {
                    dialoag.dismiss();
                    finish();
                    Toast.makeText(BarCodeActivity.this, "No response...", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ScanOrderDTO> call, Throwable t) {
                finish();
            }
        });
    }

    @Override
    public void handleResult(Result result) {
        myResult = result.getText();

        scannerView.resumeCameraPreview(BarCodeActivity.this);
        dialoag = new ProgressDialog(BarCodeActivity.this);
        dialoag.setMessage("Kindly Wait While We Send The Data.");
        dialoag.show();
        getData(data, wave);

//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Scan Result");
//        builder.setPositiveButton("Confirm ", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
////                startActivity(browserIntent);
//            }
//        });
//        builder.setNeutralButton("Visit", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(myResult));
//                startActivity(browserIntent);
//            }
//        });
//        builder.setMessage(result.getText());
//        AlertDialog alert1 = builder.create();
//        alert1.show();
    }


}
