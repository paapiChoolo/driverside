package com.example.syyam.driverlocation;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.syyam.driverlocation.Entitity.Locations;

import java.util.List;

@Dao
public interface MyDao {
    @Insert
    public void addLocations(Locations locations);

    @Query("Select * From locations")
    public List<Locations> getData();

    @Delete
    public  void delete(Locations locations);
}
