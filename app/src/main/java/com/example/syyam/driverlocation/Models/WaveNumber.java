package com.example.syyam.driverlocation.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WaveNumber {

@SerializedName("WaveNumber")
@Expose
private String waveNumber;
@SerializedName("WaveDate")
@Expose
private String waveDate;

public String getWaveNumber() {
return waveNumber;
}

public void setWaveNumber(String waveNumber) {
this.waveNumber = waveNumber;
}

public String getWaveDate() {
return waveDate;
}

public void setWaveDate(String waveDate) {
this.waveDate = waveDate;
}

}