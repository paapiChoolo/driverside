package com.example.syyam.driverlocation.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverShipmentOrderDTO {

@SerializedName("ShipmentOrder")
@Expose
private List<ShipmentOrder> shipmentOrder = null;

public List<ShipmentOrder> getShipmentOrder() {
return shipmentOrder;
}

public void setShipmentOrder(List<ShipmentOrder> shipmentOrder) {
this.shipmentOrder = shipmentOrder;
}

}