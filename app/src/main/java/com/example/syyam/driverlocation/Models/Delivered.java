package com.example.syyam.driverlocation.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Delivered {

@SerializedName("OrderNumber")
@Expose
private String orderNumber;
@SerializedName("User1")
@Expose
private String user1;

public String getOrderNumber() {
return orderNumber;
}

public void setOrderNumber(String orderNumber) {
this.orderNumber = orderNumber;
}

public String getUser1() {
return user1;
}

public void setUser1(String user1) {
this.user1 = user1;
}

}