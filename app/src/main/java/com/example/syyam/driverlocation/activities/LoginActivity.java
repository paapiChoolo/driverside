package com.example.syyam.driverlocation.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.syyam.driverlocation.API;
import com.example.syyam.driverlocation.Models.LoginDTO;
import com.example.syyam.driverlocation.Models.LoginData;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.Utils.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    private EditText userName;
    private EditText password;
    private Button signinBtn;

    public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userName=(EditText) findViewById(R.id.userNam);
        password=(EditText) findViewById(R.id.passwor);
        signinBtn=(Button) findViewById(R.id.signinBt);

        signinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    private void attemptLogin() {
        userName.setError(null);
        password.setError(null);

        String user = userName.getText().toString();
        String pass = password.getText().toString();

        View focusView = null;
        boolean cancel = false;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(pass) && !isPasswordValid(pass)) {
            password.setError("Invalid Password");
            focusView = password;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(user)) {
            userName.setError("Invalid User Name");
            focusView = userName;
            cancel = true;
        }

        if (TextUtils.isEmpty(user)) {
            password.setError("Invalid Credentials");
            focusView = password;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            LoginData loginPost = new LoginData();
            loginPost.setUserName(user);
            loginPost.setPassword(pass);
            login(loginPost);
        }
    }

    private void login(LoginData loginPost) {

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();
        Retrofit build = new Retrofit
                .Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API retrofitController = build.create(API.class);
        Call<LoginDTO> login = retrofitController.postUser(loginPost);
        login.enqueue(new Callback<LoginDTO>() {
            @Override
            public void onResponse(Call<LoginDTO> call, Response<LoginDTO> response) {
                progressDialog.dismiss();
                LoginDTO dto=  response.body();
                try {
                    Log.wtf("response", dto.getToken());
                    if (!dto.getToken().equals(null)) {
                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("token", dto.getToken());
                        editor.apply();

                        Intent Login = new Intent(LoginActivity.this, SplashActivity.class);
                        startActivity(Login);
                        finish();

                    }
                }catch (Exception e){
                        Toast.makeText(LoginActivity.this, response.body().getResponse(), Toast.LENGTH_SHORT).show();
                }
         }

            @Override
            public void onFailure(Call<LoginDTO> call, Throwable t) {
                Log.wtf("onFailure","onFailure: something went wrong"+  t.getMessage());
            }
        });
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 2;
    }
}
