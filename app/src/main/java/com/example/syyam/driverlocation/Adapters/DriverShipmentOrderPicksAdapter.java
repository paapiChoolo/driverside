package com.example.syyam.driverlocation.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syyam.driverlocation.Models.ShipmentOrder;
import com.example.syyam.driverlocation.Models.ShipmentOrderPicks;
import com.example.syyam.driverlocation.R;
import com.example.syyam.driverlocation.activities.DriverShipmentOrderActivity;
import com.example.syyam.driverlocation.activities.DriverShipmentOrderPicks;

import java.util.Collections;
import java.util.List;

public class DriverShipmentOrderPicksAdapter extends RecyclerView.Adapter<DriverShipmentOrderPicksAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    List<ShipmentOrderPicks> shipmentOrdersPicks= Collections.emptyList();
    DriverShipmentOrderPicks activity;

    public DriverShipmentOrderPicksAdapter(DriverShipmentOrderPicks context, List<ShipmentOrderPicks> shipmentOrdersPicks) {
        inflater=LayoutInflater.from(context);

        this.shipmentOrdersPicks=shipmentOrdersPicks;
        this.activity=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.drivershipmentorderpicks_comentttt_row, parent, false);
//        View view =inflater.inflate(R.layout.commentttt_row,parent,false);
        DriverShipmentOrderPicksAdapter.MyViewHolder holder=new DriverShipmentOrderPicksAdapter.MyViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull DriverShipmentOrderPicksAdapter.MyViewHolder holder, int position) {
        final ShipmentOrderPicks current= shipmentOrdersPicks.get(position);
        holder.number.setText("Order Number: "+current.getOrderNumber());
        holder.CartonLabel.setText("Carton Label: "+current.getCartonLabel());

//        holder.prLinear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                activity.method1(current.getOrderNumber());
//            }
//        });
    }


    @Override
    public int getItemCount() {
        return shipmentOrdersPicks.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        LinearLayout prLinear;
        TextView number;
        TextView CartonLabel;

        public MyViewHolder(final View itemView) {
            super(itemView);

            number=itemView.findViewById(R.id.OrderNumber);
            CartonLabel=itemView.findViewById(R.id.CartonLabel);
            prLinear=itemView.findViewById(R.id.prlinear);
        }
    }
}