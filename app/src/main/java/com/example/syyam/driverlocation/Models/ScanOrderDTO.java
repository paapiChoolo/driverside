package com.example.syyam.driverlocation.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ScanOrderDTO {

@SerializedName("Error")
@Expose
private Boolean error;
@SerializedName("Msg")
@Expose
private String msg;

public Boolean getError() {
return error;
}

public void setError(Boolean error) {
this.error = error;
}

public String getMsg() {
return msg;
}

public void setMsg(String msg) {
this.msg = msg;
}

}